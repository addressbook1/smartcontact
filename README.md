
# Address Book

There are two address books present and in each address book, you can add and save your friend's name and contact number. You can also get the unique contacts of both the address books.


## Run Locally

Clone the project

```bash
  https://gitlab.com/addressbook1/smartcontact.git
```

Go to the project directory

```bash
  cd smartcontact
```

Check branch. It should be main branch

```bash
  git status
```

Start the server

```bash
  Run the project in any Java-EE IDE (preferably Intellij). The project is built using Java 11. Please check java version of your local system.
```
View Application

```bash
  Open any browser of your choice and hit localhost:8080 & the UI will show up
```

