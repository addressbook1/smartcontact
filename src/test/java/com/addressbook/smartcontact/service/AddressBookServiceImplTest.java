package com.addressbook.smartcontact.service;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.repository.AddressBookRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class AddressBookServiceImplTest {

    @Autowired
    private AddressBookService addressBookService;

    @MockBean
    private AddressBookRepo addressBookRepo;

    @Test
    void distinctAddressBookName() {
        AddressBook addressBook1 = AddressBook.builder().addressBookName("Google").build();
        AddressBook addressBook2 = AddressBook.builder().addressBookName("Facebook").build();
        when(addressBookRepo.findAll()).thenReturn( Stream.of(addressBook1, addressBook2).collect(Collectors.toList()));
        assertEquals(2, addressBookService.distinctAddressBookName().size());
    }
}