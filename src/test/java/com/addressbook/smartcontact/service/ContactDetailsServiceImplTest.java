package com.addressbook.smartcontact.service;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.entity.ContactDetails;
import com.addressbook.smartcontact.model.ContactDetailsReqModel;
import com.addressbook.smartcontact.repository.AddressBookRepo;
import com.addressbook.smartcontact.repository.ContactDetailsRepo;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class ContactDetailsServiceImplTest {

    @Autowired
    private ContactDetailsService contactDetailsService;

    @MockBean
    private ContactDetailsRepo contactDetailsRepo;

    @MockBean
    private AddressBookRepo addressBookRepo;

    @Test
    void getAllContacts() {
        ContactDetails contact = new ContactDetails(1, "Kane", "345443234", null);
        AddressBook addressBook = AddressBook.builder().addressBookName("Google").contactDetails(List.of(contact)).build();
        when(addressBookRepo.findByAddressBookName(Mockito.any())).thenReturn(Optional.of(addressBook));
        assertEquals(1, contactDetailsService.getAllContacts("Google").size());
    }

    @Test
    void getContactById() {
        AddressBook addressBook = AddressBook.builder().addressBookName("Google").build();
        when(contactDetailsRepo.findById(Mockito.any())).thenReturn(Optional.of(new ContactDetails(1, "Alex", "98767898", addressBook)));
        assertEquals(new ContactDetails(1, "Alex", "98767898", addressBook), contactDetailsService.getContactById(1));
    }

    @Disabled
    @Test
    void addNewContact() {
        ContactDetailsReqModel contact = ContactDetailsReqModel.builder()
                .id(1)
                .name("Kane")
                .contactNumber("543444567")
                .addressBookName("Facebook")
                .build();
        when(contactDetailsRepo.save(Mockito.any())).thenReturn(contact);
        assertEquals(contact, contactDetailsService.addNewContact(contact));
    }

    @Disabled
    @Test
    void updateContact() {
        AddressBook addressBook = AddressBook.builder().addressBookName("Facebook").build();
        ContactDetailsReqModel contact = ContactDetailsReqModel.builder()
                .id(1)
                .name("Kane")
                .contactNumber("789980086")
                .addressBookName("Facebook")
                .build();
        ContactDetailsReqModel updatedContact = ContactDetailsReqModel.builder()
                .id(1)
                .name("Phil")
                .contactNumber("874933524")
                .addressBookName("Facebook")
                .build();
        ContactDetails newContactDetails = new ContactDetails();
        BeanUtils.copyProperties(contact, newContactDetails);
        newContactDetails.setAddressBook(addressBook);
        when(contactDetailsRepo.findById(Mockito.any())).thenReturn(Optional.of(newContactDetails));
        when(contactDetailsRepo.save(Mockito.any())).thenReturn(updatedContact);
        BeanUtils.copyProperties(updatedContact, contact);
        assertEquals(contact, contactDetailsService.updateContact(contact));
    }

    @Test
    void deleteContact() {
        AddressBook addressBook = AddressBook.builder().addressBookName("Facebook").build();
        ContactDetails contact = new ContactDetails(1, "Lucy", "99812564", addressBook);
        ContactDetails deletedContact = new ContactDetails(1, "Lucy", "99812564", addressBook);
        when(contactDetailsRepo.findById(1)).thenReturn(Optional.of(deletedContact));
        contactDetailsService.deleteContact(contact.getId());
        verify(contactDetailsRepo, times(1)).delete(deletedContact);
    }

}
