package com.addressbook.smartcontact.controller;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.entity.ContactDetails;
import com.addressbook.smartcontact.model.ContactDetailsReqModel;
import com.addressbook.smartcontact.model.ServiceResModel;
import com.addressbook.smartcontact.model.UniqueContacts;
import com.addressbook.smartcontact.service.AddressBookService;
import com.addressbook.smartcontact.service.ContactDetailsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ContactDetailsControllerTest {

    @InjectMocks
    private ContactDetailsController contactDetailsController;

    @Mock
    private ContactDetailsService contactDetailsService;

    @Mock
    private AddressBookService addressBookService;

    @Test
    void getAllContactsOfAddressBook() {
        List<ContactDetails> contacts = new ArrayList<>();
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.getAllContacts("Google"));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.getAllContactsOfAddressBook("Google"));
    }

    @Test
    void getContactOfAddressBookById() {
        Integer contactId = 2;
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.getContactById(contactId));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.getContactOfAddressBookById(contactId));
    }

    @Test
    void addContactToAddressBook() {
        ContactDetailsReqModel contact = ContactDetailsReqModel.builder()
                .name("Alex")
                .contactNumber("456776521")
                .addressBookName("Google")
                .build();
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.addNewContact(contact));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.addContactToAddressBook(contact));
    }

    @Test
    void updateContactInAddressBook() {
        ContactDetailsReqModel contact = ContactDetailsReqModel.builder()
                .id(5)
                .name("Kane")
                .contactNumber("322146678")
                .addressBookName("Facebook")
                .build();
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.updateContact(contact));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.updateContactInAddressBook(contact));
    }

    @Test
    void deleteContactFromFirstAddressBook() {
        ContactDetails contact = new ContactDetails();
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.deleteContact(contact.getId()));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.deleteContactFromAddressBook(contact));
    }

    @Test
    void getUniqueContacts() {
        List<UniqueContacts> contacts = new ArrayList<>();
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(contactDetailsService.getUniqueContacts());
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.getUniqueContacts());
    }

    @Test
    void addAddressBook() {
        AddressBook addressBook = new AddressBook();
        addressBook.setAddressBookName("Salesforce");
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(addressBookService.addAddressBook(addressBook));
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.addAddressBook(addressBook));
    }

    @Test
    void getDistinctAddressBooks() {
        ServiceResModel<Object> serviceResModel = new ServiceResModel<>(addressBookService.distinctAddressBookName());
        ResponseEntity response = new ResponseEntity(serviceResModel, HttpStatus.OK);
        assertEquals(response, contactDetailsController.getDistinctAddressBooks());
    }
}