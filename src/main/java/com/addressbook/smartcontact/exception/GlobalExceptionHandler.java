package com.addressbook.smartcontact.exception;

import com.addressbook.smartcontact.model.ApiErrorModel;
import com.addressbook.smartcontact.model.ServiceResModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ServiceResModel> resourceNotFoundException(ResourceNotFoundException e)
    {
        ApiErrorModel apiErrorModel = new ApiErrorModel((e.getLocalizedMessage()), 500 , "input is incorrect", LocalDateTime.now());
        return new ResponseEntity<>(new ServiceResModel(null, apiErrorModel), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<ServiceResModel> resourceAlreadyExistsException(ResourceNotFoundException e)
    {
        ApiErrorModel apiErrorModel = new ApiErrorModel((e.getLocalizedMessage()), 500 , "input is incorrect", LocalDateTime.now());
        return new ResponseEntity<>(new ServiceResModel(null, apiErrorModel), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
