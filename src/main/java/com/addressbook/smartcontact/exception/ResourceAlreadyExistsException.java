package com.addressbook.smartcontact.exception;

public class ResourceAlreadyExistsException extends RuntimeException{

    private String errMsg;
    public ResourceAlreadyExistsException(String errMsg){
        super(errMsg);
    }
}
