package com.addressbook.smartcontact.exception;

public class ResourceNotFoundException extends RuntimeException{

    private String errMsg;
    public ResourceNotFoundException(String errMsg){
        super(errMsg);
    }
}
