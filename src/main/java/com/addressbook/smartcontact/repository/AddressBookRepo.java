package com.addressbook.smartcontact.repository;

import com.addressbook.smartcontact.entity.AddressBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressBookRepo extends JpaRepository<AddressBook, String> {

    Optional<AddressBook> findByAddressBookName(String addressBookName);
}
