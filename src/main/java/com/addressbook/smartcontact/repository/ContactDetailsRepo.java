package com.addressbook.smartcontact.repository;

import com.addressbook.smartcontact.entity.ContactDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactDetailsRepo extends JpaRepository<ContactDetails, Integer> {

}
