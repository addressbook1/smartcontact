package com.addressbook.smartcontact.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContactDetailsReqModel {

    @JsonProperty(value = "id")
    private Integer id;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "contactNumber")
    private String contactNumber;

    @JsonProperty(value = "addressBookName")
    private String addressBookName;
}
