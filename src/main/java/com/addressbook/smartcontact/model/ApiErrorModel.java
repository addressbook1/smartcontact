package com.addressbook.smartcontact.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiErrorModel<T> {

    @JsonProperty("errorMessage")
    private T errorMessage;

    @JsonProperty("errorCode")
    private Integer errorCode;

    @JsonProperty("errorStatus")
    private String errorStatus;

    @JsonProperty("timestamp")
    private LocalDateTime datetime;


}
