package com.addressbook.smartcontact.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddressBookReqModel {

    @JsonProperty(value = "addressBookName")
    private String addressBookName;
}
