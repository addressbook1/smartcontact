package com.addressbook.smartcontact.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UniqueContacts {

    private String name;

    private String contactNumber;
}
