package com.addressbook.smartcontact.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ServiceResModel<T> {

    @JsonProperty("data")
    private T data;

    @JsonProperty("error")
    private ApiErrorModel error;


    public ServiceResModel(T data){
        this.data = data;
        this.error = null;
    }
}
