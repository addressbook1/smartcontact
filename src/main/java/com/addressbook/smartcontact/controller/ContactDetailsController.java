package com.addressbook.smartcontact.controller;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.entity.ContactDetails;
import com.addressbook.smartcontact.model.AddressBookReqModel;
import com.addressbook.smartcontact.model.ContactDetailsReqModel;
import com.addressbook.smartcontact.model.ServiceResModel;
import com.addressbook.smartcontact.model.UniqueContacts;
import com.addressbook.smartcontact.service.AddressBookService;
import com.addressbook.smartcontact.service.ContactDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/contact/v1")
public class ContactDetailsController {

    @Autowired
    private ContactDetailsService contactDetailsService;

    @Autowired
    private AddressBookService addressBookService;

    @GetMapping("/details/fetch-by-name")
    public ResponseEntity<ServiceResModel> getAllContactsOfAddressBook(@RequestParam String addressBookName){
        List<ContactDetails> allContacts = this.contactDetailsService.getAllContacts(addressBookName);
        return new ResponseEntity<>(new ServiceResModel(allContacts), HttpStatus.OK);
    }

    @GetMapping("/details/fetch-by-id")
    public ResponseEntity<ServiceResModel> getContactOfAddressBookById(@RequestParam Integer id) {
        ContactDetails contact = this.contactDetailsService.getContactById(id);
        return new ResponseEntity<>(new ServiceResModel(contact) , HttpStatus.OK);
    }

    @PostMapping("/details/add")
    public ResponseEntity<ServiceResModel> addContactToAddressBook(@RequestBody ContactDetailsReqModel contact) {
        ContactDetails newContact = this.contactDetailsService.addNewContact(contact);
        return new ResponseEntity<>(new ServiceResModel(newContact), HttpStatus.OK);
    }

    @PostMapping("/details/update")
    public ResponseEntity<ServiceResModel> updateContactInAddressBook(@RequestBody ContactDetailsReqModel updatedContact) {
        ContactDetails contact = this.contactDetailsService.updateContact(updatedContact);
        return new ResponseEntity<>(new ServiceResModel(contact), HttpStatus.OK);
    }

    @PostMapping("/details/delete")
    public ResponseEntity<ServiceResModel> deleteContactFromAddressBook(@RequestBody ContactDetails contact){
       String res =  this.contactDetailsService.deleteContact(contact.getId());
        return new ResponseEntity<>(new ServiceResModel(res), HttpStatus.OK);
    }

    @PostMapping("/address-book/add")
    public ResponseEntity<ServiceResModel> addAddressBook(@RequestBody AddressBook addressBook){
        AddressBook newAddressBook = this.addressBookService.addAddressBook(addressBook);
        return new ResponseEntity<>(new ServiceResModel(newAddressBook), HttpStatus.OK);
    }

    @GetMapping("/unique-contacts/fetch")
    public ResponseEntity<ServiceResModel> getUniqueContacts(){
        List<UniqueContacts> uniqueContacts = this.contactDetailsService.getUniqueContacts();
        return new ResponseEntity<>(new ServiceResModel<>(uniqueContacts), HttpStatus.OK);
    }

    @GetMapping("/distinct-address-book")
    public ResponseEntity<ServiceResModel> getDistinctAddressBooks(){
        List<String> distinctAddressBookNames = this.addressBookService.distinctAddressBookName();
        return new ResponseEntity<>(new ServiceResModel(distinctAddressBookNames), HttpStatus.OK);
    }

    @PostMapping("/address-book/delete")
    public ResponseEntity<ServiceResModel> deleteAddressBook(@RequestBody AddressBookReqModel addressBookReqModel) {

        String res = this.addressBookService.deleteAddressBook(addressBookReqModel.getAddressBookName());
        return new ResponseEntity<>(new ServiceResModel(res), HttpStatus.OK);
    }

}
