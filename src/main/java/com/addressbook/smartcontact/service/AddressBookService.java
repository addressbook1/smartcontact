package com.addressbook.smartcontact.service;

import com.addressbook.smartcontact.entity.AddressBook;
import java.util.List;

public interface AddressBookService {

    AddressBook addAddressBook(AddressBook addressBook);

    List<String> distinctAddressBookName();

    String deleteAddressBook(String addressBookName);
}
