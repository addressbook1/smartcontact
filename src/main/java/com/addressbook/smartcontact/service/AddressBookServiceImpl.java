package com.addressbook.smartcontact.service;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.exception.ResourceAlreadyExistsException;
import com.addressbook.smartcontact.exception.ResourceNotFoundException;
import com.addressbook.smartcontact.repository.AddressBookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AddressBookServiceImpl implements AddressBookService{

    @Autowired
    private AddressBookRepo addressBookRepo;

    @Override
    public AddressBook addAddressBook(AddressBook addressBook) {
        Optional<AddressBook> optionalAddressBook = this.addressBookRepo.findByAddressBookName(addressBook.getAddressBookName());
        if(optionalAddressBook.isPresent()){
            throw new ResourceAlreadyExistsException("Address-book with name " +
                    addressBook.getAddressBookName() + " already exists");
        }
        this.addressBookRepo.save(addressBook);
        return addressBook;
    }

    @Override
    public List<String> distinctAddressBookName() {
        List<String> distinctAddressBookNames = this.addressBookRepo.findAll()
                .stream()
                .map(addressBook -> addressBook.getAddressBookName())
                .collect(Collectors.toList());
        return distinctAddressBookNames;
    }

    @Override
    public String deleteAddressBook(String addressBookName) {
        AddressBook addressBook = this.addressBookRepo.findByAddressBookName(addressBookName)
                .orElseThrow(() -> new ResourceNotFoundException("No address book exists with name " + addressBookName));

        this.addressBookRepo.delete(addressBook);
        return "Address book and corresponding contact details are delete successfully";
    }


}
