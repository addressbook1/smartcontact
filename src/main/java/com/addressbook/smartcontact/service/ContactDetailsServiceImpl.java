package com.addressbook.smartcontact.service;

import com.addressbook.smartcontact.entity.AddressBook;
import com.addressbook.smartcontact.entity.ContactDetails;
import com.addressbook.smartcontact.exception.ResourceNotFoundException;
import com.addressbook.smartcontact.model.ContactDetailsReqModel;
import com.addressbook.smartcontact.model.UniqueContacts;
import com.addressbook.smartcontact.repository.AddressBookRepo;
import com.addressbook.smartcontact.repository.ContactDetailsRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;

@Service
public class ContactDetailsServiceImpl implements ContactDetailsService {

    @Autowired
    private ContactDetailsRepo contactDetailsRepo;

    @Autowired
    private AddressBookRepo addressBookRepo;


    @Override
    public List<ContactDetails> getAllContacts(String addressBookName) {
        AddressBook allContacts = this.addressBookRepo.findByAddressBookName(addressBookName)
                .orElseThrow(() -> new ResourceNotFoundException("No address book found with name = " + addressBookName));
        return allContacts.getContactDetails().stream().sorted(Comparator.comparing(ContactDetails::getName))
                .collect(Collectors.toList());
    }

    @Override
    public ContactDetails getContactById(Integer id) {
        ContactDetails contactDetails = this.contactDetailsRepo.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("No contact details found with contact id = " + id));
        return contactDetails;
    }

    @Override
    public ContactDetails addNewContact(ContactDetailsReqModel contactDetails) {
        AddressBook addressBook = this.addressBookRepo.findByAddressBookName(contactDetails.getAddressBookName())
                .orElseThrow(() -> new ResourceNotFoundException("No address book found with name = " + contactDetails.getAddressBookName()));
        ContactDetails newContactDetails = ContactDetails.builder()
                .name(contactDetails.getName())
                .contactNumber(contactDetails.getContactNumber())
                .addressBook(addressBook)
                .build();
        this.contactDetailsRepo.save(newContactDetails);
        return  newContactDetails;
    }

    @Override
    public ContactDetails updateContact(ContactDetailsReqModel updatedContact) {
        ContactDetails contactDetails = this.contactDetailsRepo.findById(updatedContact.getId()).orElseThrow(
                () -> new ResourceNotFoundException("No contact details found with contact id = " + updatedContact.getId()));
        BeanUtils.copyProperties(updatedContact, contactDetails);
        return this.contactDetailsRepo.save(contactDetails);
    }

    @Override
    public String deleteContact(Integer id) {
        ContactDetails contact = this.contactDetailsRepo.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("No contact details found with contact id = " + id));
        this.contactDetailsRepo.delete(contact);
        return "Contact deleted successfully";
    }

    @Override
    public List<UniqueContacts> getUniqueContacts() {

        List<ContactDetails> contactDetails = this.contactDetailsRepo.findAll();
        Map<String, List<ContactDetails>> addressBookContactDetails = contactDetails.stream().collect(groupingBy(contact -> contact.getAddressBook().getAddressBookName()));

        String firstAddressBookType = contactDetails.get(0).getAddressBook().getAddressBookName();
        List<ContactDetails> firstAddressBook = addressBookContactDetails.get(firstAddressBookType);
        Map<String, UniqueContacts> uniqueContacts = new HashMap<>();
        firstAddressBook.stream().forEach(contact -> uniqueContacts.put(contact.getName(), new UniqueContacts(contact.getName(), contact.getContactNumber())));

        addressBookContactDetails.entrySet().stream().forEach((addressBook) -> {
            if(!addressBook.getKey().equalsIgnoreCase(firstAddressBookType)){
                addressBook.getValue().stream().forEach(contact -> {
                    if(uniqueContacts.containsKey(contact.getName())){
                        uniqueContacts.put(contact.getName(), null);
                    }
                    else{
                        uniqueContacts.put(contact.getName(), new UniqueContacts(contact.getName(), contact.getContactNumber()));
                    }
                });
            }
        });
        return (uniqueContacts.values().stream().filter(Objects::nonNull)
                .sorted(Comparator.comparing(UniqueContacts::getName)).collect(Collectors.toList()));
    }

}
