package com.addressbook.smartcontact.service;
import com.addressbook.smartcontact.entity.ContactDetails;
import com.addressbook.smartcontact.model.ContactDetailsReqModel;
import com.addressbook.smartcontact.model.UniqueContacts;
import java.util.List;

public interface ContactDetailsService {

    List<ContactDetails> getAllContacts(String addressBookName);

    ContactDetails getContactById(Integer id);

    ContactDetails addNewContact(ContactDetailsReqModel contact);

    ContactDetails updateContact(ContactDetailsReqModel updatedContact);

    String deleteContact(Integer id);

    List<UniqueContacts> getUniqueContacts();

}
