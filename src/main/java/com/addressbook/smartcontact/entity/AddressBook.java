package com.addressbook.smartcontact.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADDRESS_BOOK")
public class AddressBook {

    @Id
    @Column(name = "ADDRESS_BOOK_NAME")
    @JsonProperty(value = "addressBookName")
    private String addressBookName;

    @JsonManagedReference
    @Column(name = "ADDRESS_BOOK_NAME")
    @OneToMany(mappedBy = "addressBook", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<ContactDetails> contactDetails = new ArrayList<>();

}
