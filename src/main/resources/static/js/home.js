$('document').ready(() => {

            $("#contactsPage").hide()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            showHomePage()

            $('#homeBtn').click(() => {
                $('#addressBookTiles').html('')
                showHomePage()
            })
            $('#contactsBtn').click(() => {
                showAddressBookButtons()
                showContactsPage()
            })
            $('#addContactsBtn').click(() => {
                showAddContactsPage()
            })

            $('#addrBook').click(() => {
                showAddressBookContact($('#addressBookOption').val())
            })

            $('#addressBookOption').click(() => {
                showAddressBookDropDown()
            })
            $('#updateDetails').click(() => {
                updateDetails()
            })

            $('#uniqueContactsBtn').click(() => {
                showUniqueContacts()
            })

            $('#addAddressBookBtn').click(() => {
                showAddAddressBookPage()
            })

            $('#addNewBookBtn').click(() => {
                addNewAddressBook()
            })

            saveContactDetails()


            callFunctionAfterTyping('updateName', validateUpdatePageName)
            callFunctionAfterTyping('updateNumber', validateUpdatePageNumber)
            callFunctionAfterTyping('contactName', validateName)
            callFunctionAfterTyping('contactNumber', validateNumber)
            callFunctionAfterTyping('addressBookName', validateAddressBookName)
            callFunctionAfterTyping('addressBookOption', validateUpdatePageDropdown)
        })

        var charExp = /([a-zA-Z~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/
        var specialChar = /([0-9~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/
        var phnNoExp =  /^(2|3|4|5|7|8)[0-9]{8}$/

        var validName = false
        var validNumber = false
        var validAddressBookName = false
        var validUpdateNameFlag = true
        var validUpdateNumberFlag = true
        var validUpdateBookFlag = true
        var dropDown = false

        var oldNameInUpdatePage
        var oldNumberInUpdatePage

        function callFunctionAfterTyping(param1, param2) {
            var typingTimer;
            var doneTypingInterval = 10;

            $('#' + param1).on('keyup', () => {

                clearTimeout(typingTimer);
                typingTimer = setTimeout(param2, doneTypingInterval);
            });

            $('#' + param1).on('keydown', () => {
                clearTimeout(typingTimer);
            })

            $('#' + param1).click(() => {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(param2, doneTypingInterval);
            });
        }

        function showContactsPage(){

            $('#contactsPage').show()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').hide()
            $('#addAddressBookPage').hide()

        }

        function showAddContactsPage(){

            $('#contactsPage').hide()
            $('#addContactsPage').show()
            $('#updateContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').hide()
            $('#addAddressBookPage').hide()
            $('#contactName').val('')
            $('#contactNumber').val('')
            $('#addressBookOption').val('')

        }

        function showHomePage() {

            $('#contactsPage').hide()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            $('#uniqueContactsPage').hide()
            $('#addAddressBookPage').hide()
            $('#homePage').show()

            var myDate = new Date();
            var hrs = myDate.getHours();
            var greet;
            if (hrs < 12) {
                greet = 'Good morning.';
            }
            else if (hrs >= 12 && hrs <= 17) {
                greet = 'Good afternoon.';
            }
            else if (hrs >= 17 && hrs <= 24) {
                greet = 'Good evening.';
            }
            $('#greet').html(`<b>${greet} Welcome to my address book</b>`)
            $('#addressBookTiles').html('')
            var addressBookType
            $.ajax({
                type: 'GET',
                url: '/contact/v1/distinct-address-book',
                dataType: 'json',
                success: function (data) {

                    var resultHtml = ``
                    addressBookType = data.data
                    for (let i = 0; i < addressBookType.length; i++) {
                        $.ajax({
                            async: false,
                            type: 'GET',
                            url: '/contact/v1/details/fetch-by-name',
                            data: { 'addressBookName': addressBookType[i] },
                            dataType: 'json',
                            success: function (data) {

                                if (i % 3 == 0) {
                                    resultHtml += `<div class="row align-items-center">`
                                }
                                resultHtml += `<div class="col-md-4 p-2">
                                                <div class="card service-card" style="box-shadow:5px 5px 5px #0047AB">
                                                    <div class="card-header p-4" style = "background-color : #0047AB">
                                                        <h6 class="card-title text-white text-center">${addressBookType[i]} Address Book &nbsp;
                                                        <a class="btn btn-danger btn-sm" onclick = deleteAddressBook('${addressBookType[i]}') id=${addressBookType[i]}>
                                                        <i class="fa-solid fa-trash"></i></a>
                                                        </h6>
                                                    </div>
                                                    <div class="card-body">
                                                        <i class="fa-solid fa-users fa-2xl" style="color: #0047ab;"></i>
                                                        <h1 id="No" class="m-2 text-center">${data.data.length}</h1>
                                                        <h5 class="text-center">Contacts</h5>
                                                    </div>
                                                </div>
                                            </div>`
                                if (i % 3 == 2) {
                                    resultHtml += `</div>`
                                }

                                $('#addressBookTiles').html(resultHtml);

                            },
                            error: function (data) {
                                console.log(data)
                            }
                        })
                    }

                },
                error: function (data) {
                    console.log(data)
                }
            })
        }

        function showAddressBookContact(param1){
            $('#contactsPage').show()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').hide()
            $('#addAddressBookPage').hide()
            var url = '/contact/v1/details/fetch-by-name/'
            $.ajax({
                type : 'GET',
                url : url,
                data : {'addressBookName' : param1},
                dataType : 'json',
                success : function(data){
                    $('#action').show()
                    var headerText = param1 + ' Address Book (Sorted by Name)'
                    $('#detailsHead').text(headerText)
                    var resultHtml = ``
                    if(data.data.length > 0){
                        for(let i=0; i<data.data.length; i++){
                            var deleteId = 'deleteContact' + data.data[i].id
                            resultHtml += `<tr>
                                <td>${i+1}</td>
                                <td>${data.data[i].name}</td>
                                <td>(+61) ${data.data[i].contactNumber}</td>
                                 <td>
                                    <a class="btn btn-outline-success" onclick = showUpdatePage(${data.data[i].id},'${data.data[i].name}','${data.data[i].contactNumber}','${param1}')><i class="fa-solid fa-pen"></i></a>
                                    <a class="btn btn-outline-danger" onclick = deleteContactDetails(${data.data[i].id},'${param1}') id=${deleteId}><i class="fa-solid fa-trash"></i></a>
                                </td>
                            </tr>`

                        }
                    }
                    else{
                        resultHtml += `<tr><td colspan="4"><center>No data Found</center></td></tr>`
                    }
                    $('#contactRecords').html(resultHtml)
                },
                error : function(data) {
                    console.log(data)
                }
            })
        }

        function showUniqueContacts() {
            $('#contactsPage').hide()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').show()
            $('#addAddressBookPage').hide()
            $.ajax({
                type: 'GET',
                url: '/contact/v1/unique-contacts/fetch',
                dataType: 'json',
                success: function (data) {

                    $('#uniqueDetailsHead').text('Unique Contacts')
                    var resultHtml = ``
                    if (data.data.length > 0) {
                        for (let i = 0; i < data.data.length; i++) {
                            resultHtml += `<tr>
                                <td>${i + 1}</td>
                                <td>${data.data[i].name}</td>
                                <td>(+61) ${data.data[i].contactNumber}</td>
                            </tr>`
                        }
                    }
                    else {
                        resultHtml += `<tr><td colspan="3"><center>No data Found</center></td></tr>`
                    }
                    $('#uniqueContactRecords').html(resultHtml)
                },
                error: function (data) {
                    console.log(data)
                }
            })

        }

        function validateAddressBookName(){
            if($('#addressBookName').val().length == 0){
                $('#validAddressBookName').html(`<span>
                   <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Address book name cannot be empty`)
                validAddressBookName = false
            }
            else{
                $('#validAddressBookName').html(``)
                validAddressBookName = true
            }
        }

        function validateUpdatePageDropdown(){
            if($('#addressBookOption').val() == null || $('#addressBookOption').val().length == 0){
                $('#validDropdownName').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Address book name cannot be empty. Choose one`)
                validUpdateBookFlag = false
            }
            else{
                $('#validDropdownName').html(``)
                validUpdateBookFlag = true
            }
        }

        function validateName(){

            if($('#contactName').val().length == 0){
                $('#validName').html(`<span>
                   <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact name cannot be empty`)
                validName = false
            }
            else{
                if($('#contactName').val().trim().match(specialChar)){
                    $('#validName').html(`<span>
                        <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                        </span>Contact name cannot contain numbers or special characters`)
                    validName = false
                }
                else{
                    $('#validName').html(``)
                    validName = true
                }
            }
        }

        function validateNumber() {
            if($('#contactNumber').val().trim().match(phnNoExp)){
                $('#validNumber').html(``)
                validNumber = true
            }
            else if ($('#contactNumber').val().length == 0) {
                $('#validNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number cannot be empty`)
                validNumber = false
            }
            else if ($('#contactNumber').val().trim().match(charExp)){
                $('#validNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number cannot contain alphabets or special characters`)
                validNumber = false
            }
            else {
                $('#validNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number is not valid`)
                validNumber = false
            }
        }

        function validateUpdatePageName(){

            if($('#updateName').val().length == 0){
                    $('#validUpdateName').html(`<span>
                       <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact name cannot be empty`)
                    validUpdateNameFlag = false
            }
            else{
                 if($('#updateName').val().trim().match(specialChar)){
                     $('#validUpdateName').html(`<span>
                          <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                          </span>Contact name cannot contain numbers or special characters`)
                      validUpdateNameFlag = false
                 }
                 else{
                      $('#validUpdateName').html(``)
                      validUpdateNameFlag = true
                 }
            }
        }

        function validateUpdatePageNumber(){
            if($('#updateNumber').val().trim().match(phnNoExp)){
                $('#validUpdateNumber').html(``)
                validUpdateNumberFlag = true
            }
            else if($('#updateNumber').val().length == 0){
                $('#validUpdateNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number cannot be empty`)
                validUpdateNumberFlag = false
            }
            else if ($('#updateNumber').val().trim().match(charExp)){
                $('#validUpdateNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number cannot contain alphabets or special characters`)
                validUpdateNumberFlag = false
            }
            else {
                $('#validUpdateNumber').html(`<span>
                    <i class="fa-solid fa-circle-exclamation fa-beat-fade" style="color: #e70d44;"></i>
                    </span>Contact number is not valid`)
                validUpdateNumberFlag = false
            }
        }

        function showUpdatePage(param1, param2, param3, param4){

            $('#contactsPage').hide()
            $('#addContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').hide()
            $('#updateContactsPage').show()
                $('#addAddressBookPage').hide()
            $('#updateId').val(param1)
            $('#updateName').val(param2)
            $('#updateNumber').val(param3)
            $('#addrBookType').val(param4)
            $('#updatePageHeader').text('Update Contact Details - ' + param4 + ' Address Book')

            oldNameInUpdatePage = $('#updateName').val()
            oldNumberInUpdatePage = $('#updateNumber').val()
        }

        function saveContactDetails(){
            $('#saveDetails').click(() => {
                if(validName && validNumber && validUpdateBookFlag){
                    $.ajax({
                        url : '/contact/v1/details/add',
                        type : 'POST',
                        data : JSON.stringify( {'name' : $('#contactName').val(),
                        'contactNumber' : $('#contactNumber').val(),
                        'addressBookName' : $('#addressBookOption').val()}),
                        contentType : 'application/json',
                        dataType : 'json',
                        success : function(data){
                            alert('Contact details is saved successfully in '
                            + $('#addressBookOption').val() + ' address book')
                            validName = false
                            validNumber = false
                            showHomePage()
                        },
                        error : function(data){
                            console.log(data)
                        }

                    })
                }

            })
        }

        function updateDetails(){
            if(oldNameInUpdatePage == $('#updateName').val() && oldNumberInUpdatePage == $('#updateNumber').val()){
                alert("No field values are changed")
                return
            }
            if(validUpdateNameFlag && validUpdateNumberFlag){

                $.ajax({
                    url : '/contact/v1/details/update',
                    contentType : 'application/json',
                    dataType : 'json',
                    type : 'POST',
                    data : JSON.stringify({'id' : parseInt($('#updateId').val()) ,
                                'name' : $('#updateName').val(),
                                'contactNumber' : $('#updateNumber').val(),
                                'addressBookName' : $('#addrBookType').val()}),
                    success : function(data){
                        alert('Contact details are updated successfully')
                        showContactsPage()
                        showAddressBookContact($('#addrBookType').val())
                    },
                    error : function(data){
                        console.log(data)
                    }
                })
            }
        }

        function deleteContactDetails(param1, param2){
            $.ajax({
                url : '/contact/v1/details/delete',
                type : 'POST',
                contentType : 'application/json',
                dataType : 'json',
                data : JSON.stringify({'id' : param1}),
                success : function(data){
                    showAddressBookContact(param2)
                },
                error : function(data){
                    console.log(data)
                }
            })

        }

        function showAddressBookDropDown() {
            dropDown = $('#addressBookOption').val() == null ? true : !dropDown
            if (dropDown) {
                $.ajax({
                    url: '/contact/v1/distinct-address-book',
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        var resultHtml = `<option disabled selected value>Select an option</option>`
                        for (let i = 0; i < data.data.length; i++) {
                            resultHtml += `<option>${data.data[i]}</option>`
                        }
                        $('#addressBookOption').html(resultHtml)
                    }
                })
            }

        }

        function showAddressBookButtons() {
            $.ajax({
                async: true,
                url: '/contact/v1/distinct-address-book',
                type: 'GET',
                dataType: 'json',
                success: function (data) {

                    var resultHtml = ``
                    for (let i = 0; i < data.data.length; i++) {
                        if (i % 3 == 0) {
                            resultHtml += `<div class="row mt-2">`
                        }
                        resultHtml += `<div class="col-md-4">
                                            <div class="card text-white align-items-center" style="background-color:#0047AB; cursor:pointer;box-shadow:5px 5px 5px grey">
                                                <div class="card-body" id=${data.data[i]} onclick = showAddressBookContact('${data.data[i]}') >
                                                    ${data.data[i]} Address Book
                                                </div>
                                            </div>
                                        </div>`
                        if (i % 3 == 2) {
                            resultHtml += `</div>`
                        }
                    }
                    resultHtml += `<div class="card m-5">
                                        <div class="card-body">
                                            <div>
                                                <h4 id="detailsHead" class="text-center"></h4>
                                            </div>
                                            <table class="table table-hover text-center">
                                                <thead class="table-dark">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Contact Number</th>
                                                    <th id="action">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="contactRecords">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>`
                    $('#addressBookBtns').html(resultHtml)
                }
            })

        }

        function showAddAddressBookPage() {
            $('#contactsPage').hide()
            $('#addContactsPage').hide()
            $('#updateContactsPage').hide()
            $('#homePage').hide()
            $('#uniqueContactsPage').hide()
            $('#addAddressBookPage').show()
            $('#addressBookName').val('')
        }

        function addNewAddressBook(){
            if(validAddressBookName){
                $.ajax({
                    url : '/contact/v1/address-book/add',
                    type : 'POST',
                    dataType : 'json',
                    contentType : 'application/json',
                    data : JSON.stringify({'addressBookName' : $('#addressBookName').val()}),
                    success : function(data){
                        alert('New address book added successfully')
                        showHomePage()
                    },
                    error : function(data){
                        console.log(data)
                    }
                })
            }
        }

        function deleteAddressBook(param1) {
            $.ajax({
                url : '/contact/v1/address-book/delete',
                type : 'POST',
                dataType : 'json',
                contentType : 'application/json',
                data : JSON.stringify({'addressBookName' : param1}),
                success : function(data){
                    alert('Address book delete successfully')
                    showHomePage()
                },
                error : function(data){
                    console.log(data)
                }
            })
        }